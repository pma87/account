// Пдключаем плагины
var gulp = require('gulp'), // Сообственно Gulp JS
	stylus = require('gulp-stylus'), // Плагин для Stylus
	nib = require('nib'), // миксины для Stylus
	prefixer = require('gulp-autoprefixer'),
	concat = require('gulp-concat'), // Склейка файлов
	gulp = require('gulp'), // gulp
	babel = require('gulp-babel'); // babel

const dirs = {
	main: {
		src: './www/src',
		dest: './www/build'
	},
}

for(var i in dirs){
	(function(dir, i){
		gulp.task('scripts-' + i, function() {
		  return gulp.src(dir.src + '/js/**/*.js')
			// .pipe(babel({
			// 	presets: ['es2015']
			// }))
		  	.on('error', console.log)
		    .pipe(gulp.dest(dir.dest + '/js/'));
		});

		gulp.task('stylus-' + i, function() {
		    gulp.src(dirs[i].src + '/style/**/*.styl')
				.pipe(stylus({
					use:nib(),
					compress:false,
					'include css':true,
				}))
			    .on('error', console.log)
				.pipe(prefixer())
			    .pipe(gulp.dest(dirs[i].dest + '/css/'));
		});

		gulp.task('tpl-' + i, function() {
		  return gulp.src(dirs[i].src + '/tpl/**/*.html')
		  	.on('error', console.log)
		    .pipe(gulp.dest(dirs[i].dest + '/tpl/'));
		});
	})(dirs[i], i);
}

gulp.task('default', function() {
	for(var i in dirs){
		(function(dir, i){
			gulp.start('scripts-' + i, 'stylus-' + i, 'tpl-' + i);

			gulp.watch(dir.src + '/js/**/*.js', function() {
				gulp.start('scripts-' + i);
			});

			gulp.watch(dir.src + '/tpl/**/*.html', function() {
				gulp.start('tpl-' + i);
			});

			gulp.watch(dir.src + '/style/**/*.styl', function() {
				gulp.start('stylus-' + i);
			});
		})(dirs[i], i);
	}
});