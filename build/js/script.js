;(function(angular){
	"use strict";

	// Инициализация приложения и подключение модулей
	let app = angular.module('app', ['ui.router', 'ngResource']);

	// Роутинг
	app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
		$urlRouterProvider.otherwise('/account/0/form');

		$stateProvider.state('app', {
			url: '',
			abstract: true,
			template: '<div ui-view></div>'
		}).state('app.account', {
			url: '/account',
			abstract: true,
			templateUrl: '/build/tpl/account.html'
		}).state('app.account.detail', {
			url: '/:accountId/:tabId',
			templateUrl: '/build/tpl/account-detail.html',
			params: {
				accountId: '0',
				tabId: 'form'
			}
		}).state('app.settings1', {
			url: '/settings1',
			templateUrl: '/build/tpl/settings1.html'
		}).state('app.settings2', {
			url: '/settings2',
			templateUrl: '/build/tpl/settings2.html'
		}).state('app.settings3', {
			url: '/settings3',
			templateUrl: '/build/tpl/settings3.html'
		});
    }]);

	// Акаунты
	app.factory('accountRes', ['$resource', function($resource) {
		return $resource('/data/account');
	}]);

	// Статусы
	app.factory('statusRes', ['$resource', function($resource) {
		return $resource('/data/account_status');
	}]);

	// Контроллер аккаунтов
	app.controller('AccountCtrl', ['$scope', '$q', '$stateParams', '$location', '$anchorScroll', 'accountRes', 'statusRes',
		function($scope, $q, $stateParams, $location, $anchorScroll, accountRes, statusRes) {

		$scope.promises = [];
		$scope.loading = true;
		$scope.accounts = [];
		$scope.statuses = [];

		// Получаем список аккаунтов
		$scope.promises.push(
			accountRes.query(function(resp){
				resp.forEach(function(account){
					// Преобразуем дату
					account.date = new Date(account.date * 1000);
					account.expired = new Date(account.expired * 1000);

					$scope.accounts.push(account);
				});
			}).$promise
		);

		// Получаем список статусов
		$scope.promises.push(
			statusRes.query(function(resp){
				$scope.statuses = resp;
			}).$promise
		);

		// Все загрузилось
		$q.all($scope.promises).then(function(){
			$scope.loading = false;
		});
	}]);

	// Контроллер аккаунтов
	app.controller('AccountDetailCtrl', ['$scope', '$q', '$stateParams', '$location', '$anchorScroll', 'accountRes', 'statusRes',
		function($scope, $q, $stateParams, $location, $anchorScroll, accountRes, statusRes) {
		$scope.account = {};

		$q.all($scope.promises).then(function(){
			$scope.account = $scope.accounts.filter(function(account){
				return account.id == $stateParams.accountId;
			})[0];
		});

		$scope.$stateParams = $stateParams;
		// Сохранение аккаунта
		$scope.saveAccount = function($event){
			$event.preventDefault();
			$event.stopPropagation();

			$scope.accountForm.successText = 'Данные успешно сохранены';

			// Скрол до сообщения формы
			$anchorScroll('accountForm');

			// Отправка на сервер (закомментировано, т.к. backend отсутствует)
			// accountRes.save($scope.account).$promise.then(function(resp){
			// 	$scope.accountForm.successText = 'Данные успешно сохранены';
			// }, function(resp){
			// 	$scope.accountForm.errorText = 'Ошибка сохранения данных';
			// });
		}
	}]);
})(window.angular);